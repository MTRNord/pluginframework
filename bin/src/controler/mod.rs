use crossbeam::scope;
use std;
use lib;
use std::result::Result;
use std::sync::Arc;

mod events;

#[derive(Debug)]
struct Controler {
    input_plugins: Vec<lib::Library>,
    output_plugins: Vec<lib::Library>,
}

impl Controler {
    pub fn new() -> Controler {
        let input_plugins = Vec::new();
        let output_plugins = Vec::new();
        Controler { input_plugins, output_plugins }
    }

    pub fn init_plugins (&mut self) {
        self.init_output_plugins();
        self.init_input_plugins();
    }

    fn start(&self, plugin_type: &str) {
        if plugin_type == "input" {
            scope(|scope| for plugin_lib in &self.input_plugins {
                scope.spawn(move || { self.start_int(plugin_lib).unwrap(); });
            });
        } else if plugin_type == "output" {
            scope(|scope| for plugin_lib in &self.output_plugins {
                scope.spawn(move || { self.start_int(plugin_lib).unwrap(); });
            });
        }
    }

    fn start_int(&self, plugin_lib: &lib::Library) -> Result<&'static str, std::io::Error> {
        unsafe {
            let plugin_lib_int = plugin_lib;
            let func: lib::Symbol<unsafe extern "C" fn() -> &'static str> =
                plugin_lib_int.get(b"plugin_init")?;
            Ok(func())
        }
    }

    fn send_event_int(&self, plugin_lib: &lib::Library, event: events::Event) -> Result<Arc<events::Event>, std::io::Error> {
        unsafe {
            let plugin_lib_int = plugin_lib;
            let func: lib::Symbol<unsafe extern "C" fn(events::Event) -> Arc<events::Event>> =
                plugin_lib_int.get(b"recv_event")?;
            Ok(func(event))
        }
    }

    fn add_input_plugin(&mut self, plugin_lib: lib::Library) {
        self.input_plugins.push(plugin_lib);
    }
    fn add_output_plugin(&mut self, plugin_lib: lib::Library) {
        self.output_plugins.push(plugin_lib);
    }

    fn send_event(&self) {
        for plugin_lib in &self.output_plugins {
            let event: events::Event = events::Event{input_source_name: "testIn", output_format:"testIn2"};
            let event_resp = self.send_event_int(plugin_lib, event).unwrap().input_source_name;
            println!("event_resp: {}", event_resp);
        }
    }

    fn get_events(&self) {
        unimplemented!()
    }

    fn init_input_plugins(&mut self) {
        use std::fs;
        use std::env;

        let paths;

        let input_plugins: &mut Controler = self;

        match env::current_exe() {
            Ok(exe_path) => paths = fs::read_dir(exe_path.parent().unwrap().to_str().unwrap()).unwrap(),
            Err(_e) => paths = fs::read_dir("./").unwrap(),
        };

        for path in paths {
            let path = path.unwrap();
            let filename_living = path.file_name();
            let filename = filename_living.to_str().unwrap();

            if filename.starts_with("input_") && filename.ends_with(".dll") ||
                filename.ends_with(".so")
                {
                    println!("{}", path.path().display());
                    let plugin_lib: lib::Library = lib::Library::new(&format!("{}", path.path().display()))
                        .unwrap();
                    input_plugins.add_input_plugin(plugin_lib);
                }
        }
        //input_plugins.start("input");
    }

    fn init_output_plugins(&mut self) {
        use std::fs;
        use std::env;

        let paths;

        let output_plugins: &mut Controler = self;

        match env::current_exe() {
            Ok(exe_path) => paths = fs::read_dir(exe_path.parent().unwrap().to_str().unwrap()).unwrap(),
            Err(_e) => paths = fs::read_dir("./").unwrap(),
        };

        for path in paths {
            let path = path.unwrap();
            let filename_living = path.file_name();
            let filename = filename_living.to_str().unwrap();

            if filename.starts_with("output_") && filename.ends_with(".dll") ||
                filename.ends_with(".so")
                {
                    println!("{}", path.path().display());
                    let plugin_lib: lib::Library = lib::Library::new(&format!("{}", path.path().display()))
                        .unwrap();
                    output_plugins.add_output_plugin(plugin_lib);
                }
        }
        //output_plugins.start("output");
    }
}

pub fn start_handler() {
    let mut controler: Controler = Controler::new();
    controler.init_plugins();
    controler.send_event();
}