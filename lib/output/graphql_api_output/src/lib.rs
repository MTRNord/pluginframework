#[macro_use] extern crate juniper;
extern crate iron;
extern crate mount;
extern crate logger;
extern crate juniper_iron;

use std::env;
mod events;

use mount::Mount;
use logger::Logger;
use iron::prelude::*;
use juniper::{Context, EmptyMutation};
use juniper_iron::{GraphQLHandler, GraphiQLHandler};
use std::collections::HashMap;
use juniper::FieldResult;

struct User { id: String, name: String, friend_ids: Vec<String>  }
struct QueryRoot;
struct Database { users: HashMap<String, User> }

graphql_object!(User: Database |&self| {
     field id() -> FieldResult<&String> {
         Ok(&self.id)
     }

     field name() -> FieldResult<&String> {
         Ok(&self.name)
     }

     field friends(&executor) -> FieldResult<Vec<&User>> {
         Ok(self.friend_ids.iter()
             .filter_map(|id| executor.context().users.get(id))
             .collect())
     }
 });

graphql_object!(QueryRoot: Database |&self| {
     field user(&executor, id: String) -> FieldResult<Option<&User>> {
         Ok(executor.context().users.get(&id))
     }
});


fn context_factory(_: &mut Request) -> Database {
    Database {
        users: vec![
            ( "1000".to_owned(), User {
                id: "1000".to_owned(), name: "Robin".to_owned(),
                friend_ids: vec!["1001".to_owned()] } ),
            ( "1001".to_owned(), User {
                id: "1001".to_owned(), name: "Max".to_owned(),
                friend_ids: vec!["1000".to_owned()] } ),
        ].into_iter().collect()
    }
}

impl Context for Database {}

#[no_mangle]
pub fn plugin_init() -> Result<&'static str, &'static str> {
    let mut mount = Mount::new();

    let graphql_endpoint = GraphQLHandler::new(
        context_factory,
        QueryRoot,
        EmptyMutation::<Database>::new(),
    );
    let graphiql_endpoint = GraphiQLHandler::new("/graphql");

    mount.mount("/", graphiql_endpoint);
    mount.mount("/graphql", graphql_endpoint);

    let (logger_before, logger_after) = Logger::new(None);

    let mut chain = Chain::new(mount);
    chain.link_before(logger_before);
    chain.link_after(logger_after);

    let host = env::var("LISTEN").unwrap_or("0.0.0.0:8080".to_owned());
    println!("GraphQL server started on {}", host);
    Iron::new(chain).http(host.as_str()).unwrap();
    Ok("Started GraphQL API Plugin")
}

use std::sync::Arc;
#[no_mangle]
pub fn recv_event(recvd_event: events::Event) -> Arc<events::Event> {
    println!("recvd_event_graphql: {}", recvd_event.input_source_name);
    let event: events::Event = events::Event{input_source_name: "testOutGraphQL", output_format: "testOutGraphQL2"};
    let arc = Arc::new(event);
    arc
}