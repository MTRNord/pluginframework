#![feature(plugin)]
#![plugin(rocket_codegen)]

extern crate rocket;
mod events;

#[no_mangle]
pub fn plugin_init() -> Result<&'static str, &'static str> {
    rocket::ignite().mount("/", routes![index]).launch();
    Ok("Started Example Web Plugin")
}

#[get("/")]
fn index() -> &'static str {
    "Hello World"
}

use std::sync::Arc;
#[no_mangle]
pub fn recv_event(recvd_event: events::Event) -> Arc<events::Event> {
    println!("recvd_event: {}", recvd_event.input_source_name);
    let event: events::Event = events::Event{input_source_name: "testOut", output_format: "testOut2"};
    Arc::new(event)
}