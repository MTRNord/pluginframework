#[repr(C)]
#[derive(Debug)]
pub struct Event {
    pub input_source_name: &'static str,
    pub output_format: &'static str,
}